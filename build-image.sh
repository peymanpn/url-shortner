#!/bin/sh

docker build -t urlshortner-api --secret id=nugetconfig,src=nuget.config --progress plain -f ./ShorteningService/Dockerfile ./ShorteningService/

docker build -t urlshortener-ui --progress plain -f Frontend/Dockerfile ./Frontend/
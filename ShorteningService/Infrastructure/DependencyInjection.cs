﻿using Microsoft.Extensions.DependencyInjection;
using ShorteningService.Application.Interfaces;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Configuration;
using Microsoft.Extensions.Configuration;

namespace ShorteningService.Infrastructure
{
    public static class DependencyInjection
    {
        public static void AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            var redisEndpoint = configuration["redis"];
            System.Console.WriteLine("Redis Endpoint: " + redisEndpoint);

            // Add the redis database connection
            services.AddSingleton(ConnectionMultiplexer.Connect(redisEndpoint));

            // Add the database
            services.AddTransient<IRepository, Repository>();
        }
    }
}
